#ifndef _TIMEOUT_H
#define _TIMEOUT_H

#include <time.h>

namespace scl
{
    class Timeout
    {
        private:
            clock_t m_time;
            int m_maxTime;
            char m_state;

        public:
            Timeout(int time);
            void start(void);
            char end(void);
    };
} // namespace scl
#endif
