#include "scl/timeout.h"
#include <stdio.h>

namespace scl
{
    Timeout::Timeout(int time)
    {
        m_maxTime = time;
        m_state = 0;
    }

    void Timeout::start(void)
    {
        m_time = clock();
        m_state = 1;
    }

    char Timeout::end(void)
    {
        if(m_state)
        {
            if(((clock() - m_time) / (double) (CLOCKS_PER_SEC / 1000)) >= m_maxTime)
            {
                m_state = 0;
                return 0;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 0;
        }
    }
    
} // namespace scl