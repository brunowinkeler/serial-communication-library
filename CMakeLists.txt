cmake_minimum_required(VERSION 3.6.2)

# Maps to a solution file (Tutorial.sln). The solution will 
# have all targets (exe, lib, dll) as projects (.vcproj)
project(serial-communication-library)
set(CMAKE_INSTALL_PREFIX "${PROJECT_SOURCE_DIR}/install" CACHE PATH "Default Install Path" FORCE)
# Turn on the ability to create folders to organize projects (.vcproj)
# It creates "CMakePredefinedTargets" folder by default and adds CMake
# defined projects like INSTALL.vcproj and ZERO_CHECK.vcproj and ALL_BUILD.vcproj
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# If on Windows, make sure all symbols are exported to create a shared lib
if(WIN32)
    # Add preprocessor definitions
    add_definitions(-DWIN32_LEAN_AND_MEAN)
    set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS 1)
endif()

# Check if compiler is MSVC or GNU
if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    # Command to output information to the console
    # Useful for displaying errors, warnings, and debugging
    message(STATUS "Setting MSVC flags")
    # Set compiler flags and options. 
    # Here it is setting the Visual Studio warning level to 4
    # Establishing default compiler as the latest version of MSVC++
    # And setting to build with multiple cores
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /EHc /MP")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    # Command to output information to the console
    # Useful for displaying errors, warnings, and debugging
    message(STATUS "Setting GNU flags")
    # Set compiler flags and options.
    # Set C++11 as standard
    set (CMAKE_CXX_STANDARD 11)
    # Here it is setting to buil with multiple cores
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -Wall")
endif()

# Define the folder of Includes, libraries and Runtime Files. (.h, .hpp, .ilk, .exe, .dll, .lib, .exp etc.)
set(PROJECT_INCLUDES "${CMAKE_INSTALL_PREFIX}/include")
set(PROJECT_LIBS     "${CMAKE_INSTALL_PREFIX}/lib")
set(PROJECT_BINARIES "${CMAKE_INSTALL_PREFIX}/bin")

# Rename generated libraries in debug case
if(NOT CMAKE_DEBUG_POSTFIX)
  set(CMAKE_DEBUG_POSTFIX "_d")
endif()

# Adds the respective subdirectories of the project
add_subdirectory(src/scl)

# Checks if tests are to be built
option(BUILD_TESTS "Build tests" OFF)
if(BUILD_TESTS)
    message(STATUS "The project will build the tests")

    # Adds the respective subdirectories of test project
    add_subdirectory(tests)

    # Assign Main as the startup project of the Solution
    set_property(DIRECTORY ${PROJECT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT scl_test)
endif(BUILD_TESTS)