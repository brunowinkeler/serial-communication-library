#include <stdio.h>
#include "scl/serial.h"

// Create serial port
scl::Serial serial1;

int main(int argc, char** argv)
{
    char buffer[256] = {0};
    int i, length = 0;
    char com_name[] = "/dev/ttyACM0";
    
    // Open serial port ("COM3", "/dev/ttyUSB0")
    serial1.Open(com_name, 115200, 8, scl::Parity::NONE, 1);
    
    while(1)
    {
        // Wait character
        length = serial1.Read(buffer);
        
        if(length)
        {
            for(i = 0; i < length; i++)
            {
                // printf("%.2X ", buffer[i]);
                printf("%c", buffer[i]);
            }
            
            printf("\n");
            
            // Send data
            serial1.Write(buffer, length);
        }
    }
    
    // Close serial port
    serial1.Close();
    
    return 0;
}
